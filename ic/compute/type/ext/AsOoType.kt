package ic.compute.type.ext


import ic.base.reflect.ext.className

import ic.compute.type.*

import ic.ooc.type.*
import ic.ooc.type.funs.unionOoTypes


val Type.asOoType : OoType get() = when (this) {

	BooleanType -> OoBooleanType

	is BranchType -> unionOoTypes(ifTrue.asOoType, ifFalse.asOoType)

	NullType -> OoNullType

	NumberType -> OoFloat64Type

	StringType -> OoStringType

	UnknownType -> OoRootType

	else -> throw NotImplementedError("this.className: ${ this.className }")

}
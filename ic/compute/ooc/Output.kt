package ic.compute.ooc


import ic.compute.Computable

import ic.ooc.expr.OoExpression
import ic.ooc.statement.OoStatement


abstract class Output {

	abstract val computable : Computable

	abstract fun generateStatement (expression: OoExpression) : OoStatement

}
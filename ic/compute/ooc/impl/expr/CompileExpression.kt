package ic.compute.ooc.impl.expr


import ic.base.reflect.ext.className
import ic.base.throwables.NotSupportedException

import ic.math.numbers.Number

import ic.compute.Computable
import ic.compute.Constant
import ic.compute.bool.*
import ic.compute.bool.Compare
import ic.compute.bool.branch.Branch
import ic.compute.number.Negate
import ic.compute.number.Product
import ic.compute.number.Sum
import ic.compute.string.Concat

import ic.ooc.expr.OoExpression
import ic.ooc.expr.literals.OoBooleanLiteral
import ic.ooc.expr.literals.OoNullLiteral
import ic.ooc.expr.literals.OoStringLiteral

import ic.compute.ooc.impl.Session
import ic.compute.ooc.impl.expr.impl.*
import ic.compute.ooc.impl.expr.impl.compileAnd
import ic.compute.ooc.impl.expr.impl.compileBranch
import ic.compute.ooc.impl.expr.impl.compileCompare
import ic.compute.ooc.impl.expr.impl.compileConcat
import ic.compute.ooc.impl.expr.impl.compileEquals
import ic.compute.ooc.impl.expr.impl.compileNot
import ic.compute.ooc.impl.expr.impl.compileNumberConstant
import ic.compute.ooc.impl.expr.impl.compileSum
import ic.struct.collection.ext.reduce.find.findOrNull


internal fun Session.compileExpression (

	computable : Computable

) : OoExpression {

	return when (computable) {

		is ic.compute.Input -> inputs.findOrNull { it.undefinedComputableName == computable.name }?.expression ?: OoNullLiteral

		is Constant -> when (val constantValue = computable.value) {
			null -> OoNullLiteral
			is Boolean -> OoBooleanLiteral(constantValue)
			is Number -> compileNumberConstant(constantValue)
			is String -> OoStringLiteral(constantValue)
			else -> throw NotSupportedException.Runtime(
				message = "constantValue.className: ${ constantValue.className }"
			)
		}

		is And 	   -> compileAnd     (computable)
		is Branch -> compileBranch  (computable)
		is Compare -> compileCompare (computable)
		is Concat  -> compileConcat  (computable)
		is Equals  -> compileEquals  (computable)
		is Negate  -> compileNegate  (computable)
		is Not	   -> compileNot     (computable)
		is Product -> compileProduct (computable)
		is Sum     -> compileSum     (computable)

		else -> throw NotImplementedError("computable.className: ${ computable.className }")

	}

}
package ic.compute.ooc.impl.expr.impl


import ic.compute.ooc.impl.Session
import ic.compute.ooc.impl.expr.compileExpression
import ic.compute.string.Concat
import ic.compute.struct.ListConstant

import ic.ooc.expr.OoExpression
import ic.ooc.expr.OoConcatExpression
import ic.struct.list.ext.copy.convert.copyConvert


internal fun Session.compileConcat (

	computable : Concat

) : OoExpression {

	return saveToVariable(computable) {

		val operands = (computable.operandsList as ListConstant).items

		OoConcatExpression(
			operands.copyConvert { operand ->
				compileExpression(operand)
			}
		)

	}

}
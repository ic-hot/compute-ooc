package ic.compute.ooc.impl.expr.impl


import ic.compute.bool.Equals
import ic.compute.ooc.impl.Session
import ic.compute.ooc.impl.expr.compileExpression

import ic.ooc.expr.OoEqualsExpression
import ic.ooc.expr.OoExpression


internal fun Session.compileEquals (

	computable : Equals

) : OoExpression {

	return saveToVariable(computable) {

		OoEqualsExpression(
			a = compileExpression(computable.a),
			b = compileExpression(computable.b)
		)

	}

}
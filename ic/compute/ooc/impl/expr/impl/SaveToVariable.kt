package ic.compute.ooc.impl.expr.impl


import ic.compute.Computable
import ic.compute.ooc.impl.Session
import ic.compute.type.ext.asOoType

import ic.ooc.expr.OoExpression
import ic.ooc.expr.OoLocalValueExpression
import ic.ooc.statement.OoValueDefinition


internal inline fun Session.saveToVariable (

	computable : Computable,

	crossinline implementCompileExpression : () -> OoExpression

) : OoExpression {

	savedVariables[computable]?.let { return it }

	val variableName = "memo${ nextVariableIndex++ }"

	val ooType = computable.type.asOoType

	statements.add(
		OoValueDefinition(
			name = variableName,
			type = ooType,
			value = implementCompileExpression()
		)
	)

	val valueExpression = OoLocalValueExpression(
		localValueName = variableName,
		type = ooType
	)

	savedVariables[computable] = valueExpression

	return valueExpression

}
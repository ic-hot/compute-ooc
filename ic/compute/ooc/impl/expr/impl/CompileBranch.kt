package ic.compute.ooc.impl.expr.impl


import ic.compute.bool.branch.Branch
import ic.compute.ooc.impl.Session
import ic.compute.ooc.impl.expr.compileExpression

import ic.ooc.expr.OoBranchExpression
import ic.ooc.expr.OoExpression


internal fun Session.compileBranch (

	computable : Branch

) : OoExpression {

	return saveToVariable(computable) {

		OoBranchExpression(
			condition = compileExpression(
				computable.condition
			),
			ifTrue = compileExpression(
				computable.ifTrue
			),
			ifFalse = compileExpression(
				computable.ifFalse
			)
		)

	}

}
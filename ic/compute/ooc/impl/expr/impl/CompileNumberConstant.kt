package ic.compute.ooc.impl.expr.impl


import ic.base.reflect.ext.className

import ic.math.numbers.Number
import ic.math.numbers.real.integer.Integer
import ic.math.numbers.real.integer.ext.asInt64

import ic.ooc.expr.OoExpression
import ic.ooc.expr.literals.OoInt64Literal


internal fun compileNumberConstant (number: Number) : OoExpression {

	if (number is Integer) {
		return OoInt64Literal(number.asInt64)
	}

	else throw NotImplementedError("number.className: ${ number.className }")

}
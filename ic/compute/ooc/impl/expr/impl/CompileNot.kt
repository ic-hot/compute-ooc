package ic.compute.ooc.impl.expr.impl


import ic.compute.bool.Not
import ic.compute.ooc.impl.Session
import ic.compute.ooc.impl.expr.compileExpression

import ic.ooc.expr.OoExpression
import ic.ooc.expr.OoNotExpression


internal fun Session.compileNot (computable: Not) : OoExpression {

	return saveToVariable(computable) {

		OoNotExpression(
			a = compileExpression(computable.operand),
		)

	}

}
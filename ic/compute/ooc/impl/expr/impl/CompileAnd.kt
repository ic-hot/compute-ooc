package ic.compute.ooc.impl.expr.impl


import ic.compute.bool.And
import ic.compute.struct.CollectionConstant

import ic.compute.ooc.impl.Session
import ic.compute.ooc.impl.expr.compileExpression

import ic.ooc.expr.OoAndExpression
import ic.ooc.expr.OoExpression
import ic.struct.collection.ext.copy.copyConvertToList


internal fun Session.compileAnd (

	computable : And

) : OoExpression {

	return saveToVariable(computable) {

		val operands = (computable.operandsCollection as CollectionConstant).items

		OoAndExpression(
			operands.copyConvertToList { operand ->
				compileExpression(operand)
			}
		)

	}

}
package ic.compute.ooc.impl.expr.impl


import ic.compute.number.Product
import ic.compute.struct.CollectionConstant

import ic.compute.ooc.impl.Session
import ic.compute.ooc.impl.expr.compileExpression

import ic.ooc.expr.OoExpression
import ic.ooc.expr.OoProductExpression
import ic.struct.collection.ext.copy.copyConvert


internal fun Session.compileProduct (

	computable : Product

) : OoExpression {

	return saveToVariable(computable) {

		val operands = (computable.operandsCollection as CollectionConstant).items

		OoProductExpression(
			operands.copyConvert { operand ->
				compileExpression(operand)
			}
		)

	}

}
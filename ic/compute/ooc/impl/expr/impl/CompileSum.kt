package ic.compute.ooc.impl.expr.impl


import ic.compute.number.Sum
import ic.compute.struct.CollectionConstant

import ic.compute.ooc.impl.Session
import ic.compute.ooc.impl.expr.compileExpression

import ic.ooc.expr.OoExpression
import ic.ooc.expr.OoSumExpression
import ic.struct.collection.ext.copy.copyConvert


internal fun Session.compileSum (

	computable : Sum

) : OoExpression {

	return saveToVariable(computable) {

		val operands = (computable.operandsCollection as CollectionConstant).items

		OoSumExpression(
			operands.copyConvert { operand ->
				compileExpression(operand)
			}
		)

	}

}
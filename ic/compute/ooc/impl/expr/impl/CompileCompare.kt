package ic.compute.ooc.impl.expr.impl


import ic.compute.bool.Compare

import ic.ooc.expr.OoExpression
import ic.ooc.expr.OoCompareExpression

import ic.compute.ooc.impl.Session
import ic.compute.ooc.impl.expr.compileExpression


internal fun Session.compileCompare (

	computable : Compare

) : OoExpression {

	return saveToVariable(computable) {

		OoCompareExpression(
			lesser = compileExpression(computable.lesser),
			greater = compileExpression(computable.greater)
		)

	}

}
package ic.compute.ooc.impl.expr.impl


import ic.compute.number.Negate
import ic.compute.ooc.impl.Session
import ic.compute.ooc.impl.expr.compileExpression

import ic.ooc.expr.OoExpression
import ic.ooc.expr.OoNegateExpression


internal fun Session.compileNegate (computable: Negate) : OoExpression {

	return saveToVariable(computable) {

		OoNegateExpression(
			a = compileExpression(computable.operand),
		)

	}

}
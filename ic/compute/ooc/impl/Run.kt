package ic.compute.ooc.impl


import ic.compute.ooc.impl.expr.compileExpression

import ic.ooc.program.OoProgram
import ic.struct.collection.ext.foreach.breakableForEach


internal fun Session.run() : OoProgram {

	outputs.breakableForEach { output ->

		val expression = compileExpression(
			computable = output.computable
		)

		statements.add(
			output.generateStatement(expression = expression)
		)

	}

	return OoProgram(statements = statements)

}
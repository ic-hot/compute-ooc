package ic.compute.ooc.impl


import ic.base.primitives.int32.Int32
import ic.struct.collection.Collection
import ic.struct.map.editable.EditableMap
import ic.struct.list.editable.EditableList

import ic.compute.Computable

import ic.ooc.expr.OoLocalValueExpression
import ic.ooc.statement.OoStatement

import ic.compute.ooc.Input
import ic.compute.ooc.Output


internal class Session (

	val inputs  : Collection<Input>, 
	val outputs : Collection<Output>
	
) {

	val statements = EditableList<OoStatement>()

	var nextVariableIndex : Int32 = 1

	val savedVariables = EditableMap<Computable, OoLocalValueExpression>()
	
}
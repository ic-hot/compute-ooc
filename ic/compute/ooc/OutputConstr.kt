package ic.compute.ooc


import ic.compute.Computable

import ic.ooc.expr.OoExpression
import ic.ooc.statement.OoStatement


inline fun Output (

	computable : Computable,

	crossinline generateStatement : (OoExpression) -> OoStatement

) : Output {

	return object : Output() {

		override val computable get() = computable

		override fun generateStatement (expression: OoExpression) = generateStatement(expression)

	}

}
@file:Suppress("NOTHING_TO_INLINE")


package ic.compute.ooc


import ic.struct.collection.Collection

import ic.compute.ooc.impl.Session
import ic.compute.ooc.impl.run

import ic.ooc.program.OoProgram


fun compileComputablesToOoProgram (

	inputs : Collection<Input>,

	outputs : Collection<Output>

) : OoProgram {
	
	return Session(inputs = inputs, outputs = outputs).run()

}
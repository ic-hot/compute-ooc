package ic.compute.ooc


import ic.ooc.expr.OoExpression


data class Input (

	val undefinedComputableName : String,

	val expression : OoExpression

)